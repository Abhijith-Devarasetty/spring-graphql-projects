package com.company.app.model;

import java.util.*;
import java.sql.*;

import javax.persistence.*;

@Entity
public class Restaurant {

	@Id
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String ConsumerStatus;

	public String getConsumerStatus() {
		return ConsumerStatus;
	}

	public void setConsumerStatus(String ConsumerStatus) {
		this.ConsumerStatus = ConsumerStatus;
	}




	public Restaurant() {  }

	public Restaurant(int id) {
		this.id = id;
	}

	public Restaurant( 
        int id, 
        String name, 
        String ConsumerStatus
	) {
		this.id = id;
		this.name = name;
		this.ConsumerStatus = ConsumerStatus;
		
	}
}
