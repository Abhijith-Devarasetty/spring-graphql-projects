package com.company.app.model;

import java.util.*;
import java.sql.*;

import javax.persistence.*;

@Entity
@Table( name = "consumer")
public class Consumer {

	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	private String lastName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private int sal;

	public int getSal() {
		return sal;
	}

	public void setSal(int sal) {
		this.sal = sal;
	}




	public Consumer() {  }

	public Consumer(String id) {
		this.id = id;
	}

	public Consumer( 
        String id, 
        String firstName, 
        String lastName, 
        int sal
	) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sal = sal;
		
	}
}
