package com.company.app.service;

import com.apollographql.federation.graphqljava.Federation;
import com.apollographql.federation.graphqljava._Entity;
import com.coxautodev.graphql.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.company.app.model.Consumer;
import com.company.app.model.Restaurant;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Configuration
public class GraphQLConfig {
    @Bean
    public GraphQLSchema customSchema(SchemaParser schemaParser, RestaurantService restaurantService, ConsumerService consumerService){
        return Federation.transform(schemaParser.makeExecutableSchema())
                .fetchEntities(env -> env.<List<Map<String, Object>>>getArgument(_Entity.argumentName)
                        .stream()
                        .map(values -> {
                            if ("Restaurant".equals(values.get("__typename"))) {
                                final Object id = values.get("id");
                                if (id instanceof Integer) {
                                    return restaurantService.lookupRestaurant((Integer) id);
                                }
                            }
                            if ("Consumer".equals(values.get("__typename"))) {
                              final Object id = values.get("id");
                              if (id instanceof String) {
                                  return consumerService.lookupConsumer((String) id);
                              }
                          }
                            return null;
                        })
                        .collect(Collectors.toList()))
                .resolveEntityType(env -> {
                    final Object src = env.getObject();
                    if (src instanceof Restaurant) {
                        return env.getSchema().getObjectType("Restaurant");
                    }
                    if (src instanceof Consumer) {
                      return env.getSchema().getObjectType("Consumer");
                    }
                    return null;
                })
                .build();
    }
}
