package com.company.app.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.company.app.model.Restaurant;
import com.company.app.repo.RestaurantRepo;

@Service
public class RestaurantService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RestaurantRepo repo;

	public Restaurant lookupRestaurant( int id) {
        return new Restaurant();
    }
	
	public List<Restaurant> getAllRestaurant(){
		log.info("service get all Restaurant method");
		return repo.findAll();
	}

	public Restaurant createRestaurant(Restaurant restaurant) {
		log.info("service add Restaurant method");
		return repo.save(restaurant);
    }
    
    public Restaurant updateRestaurant(Restaurant restaurant) {
		log.info("service update Restaurant method");
		return repo.save(restaurant);
	}

	public Optional<Restaurant> getRestaurantById(int id) {
		log.info("service find by id Restaurant method");
		return repo.findById(id);
	}

	public void deleteRestaurantById(int id) {
		try{
			log.info("service delete Restaurant method");
			repo.deleteById(id);
		} catch(Exception e){
			
		}
	}

}
