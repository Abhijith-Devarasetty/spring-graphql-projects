package com.company.app.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.company.app.model.Consumer;

@Repository
public interface ConsumerRepo extends JpaRepository<Consumer, String>{

}
