package com.company.app.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.company.app.model.Consumer;
import com.company.app.service.ConsumerService;
import com.company.app.model.Restaurant;
import com.company.app.service.RestaurantService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class Query implements GraphQLQueryResolver {

	private ConsumerService consumerService;
	private RestaurantService restaurantService;

	@Autowired
	public Query(

	ConsumerService consumerService,
	RestaurantService restaurantService
		) {
	this.consumerService = consumerService;
	this.restaurantService = restaurantService;
	}

	public Iterable<Consumer> findAllConsumer() {
		return consumerService.getAllConsumer();
	}
	public Iterable<Restaurant> findAllRestaurant() {
		return restaurantService.getAllRestaurant();
	}

}
