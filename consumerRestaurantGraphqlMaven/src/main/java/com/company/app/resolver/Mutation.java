package com.company.app.resolver;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.company.app.model.Consumer;
import com.company.app.service.ConsumerService;
import com.company.app.model.Restaurant;
import com.company.app.service.RestaurantService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;


@Component
public class Mutation implements GraphQLMutationResolver {

	private ConsumerService consumerService;
	private RestaurantService restaurantService;

	@Autowired
	public Mutation(

	ConsumerService consumerService,
	RestaurantService restaurantService
		) {
	this.consumerService = consumerService;
	this.restaurantService = restaurantService;
	}

	public Consumer createConsumer(
      String id,
      String firstName,
      String lastName,
      int sal
	) {
		Consumer consumer = new Consumer();
		consumer.setId(id);
		consumer.setFirstName(firstName);
		consumer.setLastName(lastName);
		consumer.setSal(sal);
		

		consumerService.createConsumer(consumer);

		return consumer;
	}

	public boolean deleteConsumer(String id) {
		consumerService.deleteConsumerById(id);
		return true;
	}

	public Consumer updateConsumer(
		String id,
		String firstName,
		String lastName,
		int sal
	) {
		Optional<Consumer> obj = consumerService.getConsumerById(id);

		if (obj.isPresent()) {
			Consumer consumer = obj.get();

				consumer.setFirstName(firstName);
				consumer.setLastName(lastName);
				consumer.setSal(sal);

			consumerService.updateConsumer(consumer);
			return consumer;
		}

		Consumer consumer = new Consumer();

		return consumer;
	}
	public Restaurant createRestaurant(
      int id,
      String name,
      String ConsumerStatus
	) {
		Restaurant restaurant = new Restaurant();
		restaurant.setId(id);
		restaurant.setName(name);
		restaurant.setConsumerStatus(ConsumerStatus);
		

		restaurantService.createRestaurant(restaurant);

		return restaurant;
	}

	public boolean deleteRestaurant(int id) {
		restaurantService.deleteRestaurantById(id);
		return true;
	}

	public Restaurant updateRestaurant(
		int id,
		String name,
		String ConsumerStatus
	) {
		Optional<Restaurant> obj = restaurantService.getRestaurantById(id);

		if (obj.isPresent()) {
			Restaurant restaurant = obj.get();

				restaurant.setName(name);
				restaurant.setConsumerStatus(ConsumerStatus);

			restaurantService.updateRestaurant(restaurant);
			return restaurant;
		}

		Restaurant restaurant = new Restaurant();

		return restaurant;
	}

}
