package com.company.app.model;

import java.util.*;
import java.sql.*;

import javax.persistence.*;

@Entity
public class Consumer {

	@Id
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	private String lastName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private int sal;

	public int getSal() {
		return sal;
	}

	public void setSal(int sal) {
		this.sal = sal;
	}




	public Consumer() {  }

	public Consumer(Integer id) {
		this.id = id;
	}

	public Consumer( 
        int id, 
        String firstName, 
        String lastName, 
        int sal
	) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sal = sal;
		
	}
}
