package com.company.app.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.company.app.repo.ConsumerRepo;
import com.company.app.model.Consumer;

@Service
public class ConsumerService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ConsumerRepo repo;

	public Consumer lookupConsumer(Integer id) {
        return new Consumer();
    }
	
	public List<Consumer> getAllConsumer(){
		log.info("service get all Consumer method");
		return repo.findAll();
	}

	public Consumer createConsumer(Consumer consumer) {
		log.info("service add Consumer method");
		return repo.save(consumer);
    }
    
    public Consumer updateConsumer(Consumer consumer) {
		log.info("service update Consumer method");
		return repo.save(consumer);
	}

	public Optional<Consumer> getConsumerById(Integer id) {
		log.info("service find by id Consumer method");
		return repo.findById(id);
	}

	public void deleteConsumerById(Integer id) {
		try{
			log.info("service delete Consumer method");
			repo.deleteById(id);
		} catch(Exception e){
			
		}
	}

}
