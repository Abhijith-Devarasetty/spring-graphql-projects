package com.company.app;

import com.apollographql.federation.graphqljava.Federation;
import com.apollographql.federation.graphqljava._Entity;
import com.coxautodev.graphql.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.company.app.service.ConsumerService;
import com.company.app.service.RestaurantService;
import com.company.app.model.Restaurant;
import com.company.app.model.Consumer;

@Configuration
public class GraphQLConfig {
    @Bean
    public GraphQLSchema customSchema(SchemaParser schemaParser,
ConsumerService consumerService ,
RestaurantService restaurantService 
){
        return Federation.transform(schemaParser.makeExecutableSchema())
                .fetchEntities(env -> env.<List<Map<String, Object>>>getArgument(_Entity.argumentName)
                        .stream()
                        .map(values -> {
                            if ("Consumer".equals(values.get("__typename"))) {
                                final Object id = values.get("id");
                                if (id instanceof Integer) {
                                    return consumerService.lookupConsumer((Integer) id);
                                }
                            }
                            if ("Restaurant".equals(values.get("__typename"))) {
                                final Object id = values.get("id");
                                if (id instanceof Integer) {
                                    return restaurantService.lookupRestaurant((Integer) id);
                                }
                            }
                            return null;
                        })
                        .collect(Collectors.toList()))
                .resolveEntityType(env -> {
                    final Object src = env.getObject();
                    if (src instanceof Consumer) {
                        return env.getSchema().getObjectType("Consumer");
                    }
                    if (src instanceof Restaurant) {
                        return env.getSchema().getObjectType("Restaurant");
                    }
                    return null;
                })
                .build();
    }
}
