package com.company.app;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import com.company.app.model.Consumer;
import com.company.app.model.Restaurant;
import com.company.app.service.RestaurantService;
import com.company.app.service.ConsumerService;

@Service
public class Query implements GraphQLQueryResolver {

	@Autowired
	ConsumerService consumerService;

	@Autowired
	RestaurantService restaurantService;

	public Iterable<Consumer> findAllConsumer(final DataFetchingEnvironment dataFetchingEnvironment) {
		return consumerService.getAllConsumer();
	}
	public Iterable<Restaurant> findAllRestaurant(final DataFetchingEnvironment dataFetchingEnvironment) {
		return restaurantService.getAllRestaurant();
	}

}